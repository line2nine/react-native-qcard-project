import React from 'react'
import {StyleSheet, View, Image, Text} from 'react-native'
import Background from "../components/Background/Background";
import Logo from "../components/Logo/Logo";
import Button from "../components/Button/Button";
import {theme} from "../core/theme";
import Paragraph from "../components/Text/Paragraph";
import Footer from "../components/Footer/Footer";
import Header from "../components/Header/Header";

const StartScreen = ({navigation}) => (
    <Background>
        <Logo/>
        <Header>
            당신의 감성을 기획하고
        </Header>
        <Header>
            디자인으로 꿈을 실현하세요
        </Header>
        <View style={styles.row}>
            <Button
                mode="outlined"
                style={styles.halfBorGrey}
                onPress={() => navigation.navigate('RegisterScreen')}
            >
                회원가입
            </Button>
            <Button
                mode="outlined"
                style={styles.halfBorGrey}
                onPress={() => alert('Guess Entry')}
            >
                게스트 입장
            </Button>
        </View>
        <Button
            style={styles.fullOrange}
            mode="contained" onPress={() => navigation.navigate('LoginScreen')}
        >
            로그인
        </Button>
        <Footer>
            Copyright 2016 Seoul System corp. All rights reserved.
        </Footer>
    </Background>
)

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 15
    },
    footer: {
        fontSize: 11,
        lineHeight: 21,
        textAlign: 'center',
        marginBottom: 10,
        color: '#999999',
        marginTop: 10,
    },
    fullOrange: {
        backgroundColor: '#FF9F33',
    },
    halfBorGrey: {
        width: 170,
        margin: 4,
        backgroundColor: '#eee'
    },
})

export default StartScreen
