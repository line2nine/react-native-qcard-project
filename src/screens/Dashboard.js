import React from 'react'
import Background from '../components/Background/Background'
import Logo from '../components/Logo/Logo'
import Header from '../components/Header/Header'
import Paragraph from '../components/Text/Paragraph'
import Button from '../components/Button/Button'
import axios from "axios";
import {useCookies} from "react-cookie";
import * as ApiCaller from '../helpers/Method';

const Dashboard = ({navigation}) => {
    const [cookies, setCookie, removeCookie] = useCookies([""]);
    const token = cookies.access_token;

    const onLogoutPressed = async () => {
        ApiCaller.handleGet("auth/logout", token)
            .then((res) => {
                console.log('Logged out');
                // removeCookie("access_token", {path: "/"});
                // removeCookie("user_info", {path: "/"});
                navigation.reset({
                    index: 0,
                    routes: [{name: 'LoginScreen'}],
                })
            })
            .catch((err) => {
                console.log('Fail');
                console.log(err);
            });
    };

    return (
        <Background>
            <Logo/>
            <Header>Logged In</Header>
            <Button
                mode="contained"
                style={{backgroundColor: '#FF9F33'}}
                onPress={onLogoutPressed}
            >
                Logout
            </Button>
        </Background>
    )
}

export default Dashboard
