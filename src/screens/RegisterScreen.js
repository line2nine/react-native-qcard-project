import React, {useState} from 'react'
import axios from "axios";
import {useCookies} from 'react-cookie';
import {TouchableOpacity, StyleSheet, View, Image} from 'react-native';
import {Text} from 'react-native-paper';
import Background from '../components/Background/Background';
import Logo from '../components/Logo/Logo';
import Button from '../components/Button/Button';
import TextInput from '../components/Input/TextInput';
import {theme} from '../core/theme';
import * as Setting from '../helpers/Setting';
import * as ApiCaller from "../helpers/Method";

const RegisterScreen = ({navigation}) => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [passwordConfirm, setPasswordConfirm] = useState("");
    const [name, setName] = useState("");
    const [userInfo, setUserInfo] = useCookies([""]);
    const [cookies, setCookie, removeCookie] = useCookies([""]);

    const onSignupPressed = async () => {
        const data = {
            email: email,
            password: password,
            password_confirmation: passwordConfirm,
            name: name,
        }
        ApiCaller.handleAuth("auth/signup", data)
            .then((res) => {
                // console.log(res);
                // setCookie("access_token", res.data.access_token, {path: "/"});
                // setUserInfo("user_info", res.data.user, {path: "/"});
                navigation.navigate('Dashboard');
            })
            .catch((err) => {
                alert('Wrong Email Or Password');
                console.log(err);
            });
    }

    return (
        <Background>
            <TextInput
                label="아이디 (이메일)"
                returnKeyType="next"
                value={email.value}
                onChangeText={(text) => setEmail(text)}
                autoCapitalize="none"
                autoCompleteType="email"
                textContentType="emailAddress"
                keyboardType="email-address"
                name="email"
            />
            <TextInput
                label="비밀번호"
                returnKeyType="next"
                value={password.value}
                onChangeText={(text) => setPassword(text)}
                secureTextEntry
                name="password"
            />
            <TextInput
                label="비밀번호 확인"
                returnKeyType="next"
                value={passwordConfirm.value}
                onChangeText={(text) => setPasswordConfirm(text)}
                secureTextEntry
                name="password_confirmation"
            />
            <TextInput
                label="이름"
                returnKeyType="done"
                value={name.value}
                onChangeText={(text) => setName(text)}
                name="name"
            />
            <View style={styles.row}>
                <Button labelStyle={styles.btnText} style={styles.halfBgOrange} mode="outlined" onPress={onSignupPressed}>
                    다음
                </Button>
                <Button labelStyle={styles.btnText2} style={styles.halfBgGrey} mode="outlined" onPress={navigation.goBack}>
                    취소
                </Button>
            </View>
        </Background>
    )
}

const styles = StyleSheet.create({
    forgotPassword: {
        width: '100%',
        alignItems: 'flex-end',
        marginBottom: 24,
    },
    row: {
        flexDirection: 'row',
        marginTop: 30,
    },
    forgot: {
        fontSize: 13,
        color: theme.colors.secondary,
    },
    link: {
        fontWeight: 'bold',
        color: theme.colors.primary,
    },
    halfBgGrey: {
        width: '50%',
        backgroundColor: '#cccccc'
    },
    halfBgOrange: {
        width: '50%',
        backgroundColor: '#FF9F33',
    },
    btnText: {
        color: '#fff',
        fontWeight: 'normal',
        fontSize: 18,
        lineHeight: 26,
    },
    btnText2: {
        color: '#666',
        fontWeight: 'normal',
        fontSize: 18,
        lineHeight: 26,
    }
})

export default RegisterScreen
