import React, {useState} from 'react'
import axios from "axios";
import {useCookies} from 'react-cookie';
import {TouchableOpacity, StyleSheet, View, Image} from 'react-native';
import {Text} from 'react-native-paper';
import Background from '../components/Background/Background';
import Logo from '../components/Logo/Logo';
import Button from '../components/Button/Button';
import TextInput from '../components/Input/TextInput';
import {theme} from '../core/theme';
import * as Setting from '../helpers/Setting';
import * as ApiCaller from "../helpers/Method";

const LoginScreen = ({navigation}) => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [userInfo, setUserInfo] = useCookies([""]);
    const [cookies, setCookie, removeCookie] = useCookies([""]);

    const onLoginPressed = async () => {
        const data = {
            email: email,
            password: password,
        }
        ApiCaller.handleAuth("auth/login", data)
            .then((res) => {
                // console.log(res);
                setCookie("access_token", res.data.access_token, {path: "/"});
                setUserInfo("user_info", res.data.user, {path: "/"});
                navigation.navigate('Dashboard');
            })
            .catch((err) => {
                alert('Wrong Email Or Password');
                console.log(err);
            });
    }

    return (
        <Background>
            <Logo/>
            <TextInput
                label="이메일"
                returnKeyType="next"
                value={email.value}
                onChangeText={(text) => setEmail(text)}
                error={!!email.error}
                errorText={email.error}
                autoCapitalize="none"
                autoCompleteType="email"
                textContentType="emailAddress"
                keyboardType="email-address"
                name="email"
                mode='outlined'
            />
            <TextInput
                label="비밀번호"
                returnKeyType="done"
                value={password.value}
                onChangeText={(text) => setPassword(text)}
                error={!!password.error}
                errorText={password.error}
                secureTextEntry
                name="password"
                mode='outlined'
            />
            <Button style={{backgroundColor: '#FF9F33'}} mode="contained" onPress={onLoginPressed}>
                로그인
            </Button>
        </Background>
    )
}

// const styles = StyleSheet.create({
//     forgotPassword: {
//         width: '100%',
//         alignItems: 'flex-end',
//         marginBottom: 24,
//     },
//     row: {
//         flexDirection: 'row',
//         marginTop: 4,
//     },
//     forgot: {
//         fontSize: 13,
//         color: theme.colors.secondary,
//     },
//     link: {
//         fontWeight: 'bold',
//         color: theme.colors.primary,
//     },
// })

export default LoginScreen
