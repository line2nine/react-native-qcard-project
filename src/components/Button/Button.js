import React from 'react'
import {StyleSheet} from 'react-native'
import {Button as PaperButton} from 'react-native-paper'
import {theme} from '../../core/theme'

const Button = ({mode, style, ...props}) => (
    <PaperButton
        style={[
            styles.button,
            style,
        ]}
        labelStyle={styles.text}
        mode={mode}
        {...props}
    />
)

const styles = StyleSheet.create({
    button: {
        width: '100%',
        marginVertical: 10,
        paddingVertical: 2,
    },
    text: {
        fontWeight: 'normal',
        fontSize: 18,
        lineHeight: 26,
    },
})

export default Button
