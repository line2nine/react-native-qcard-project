import React from 'react'
import {StyleSheet} from 'react-native'
import {Text} from 'react-native-paper'

const Footer = (props) => <Text style={styles.text} {...props} />

const styles = StyleSheet.create({
    text: {
        fontSize: 11,
        lineHeight: 21,
        textAlign: 'center',
        marginBottom: 5,
        color: '#999'
    },
})

export default Footer
