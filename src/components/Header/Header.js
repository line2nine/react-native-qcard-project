import React from 'react'
import {StyleSheet} from 'react-native'
import {Text} from 'react-native-paper'

const Header = (props) => <Text style={styles.text} {...props} />

const styles = StyleSheet.create({
    text: {
        fontSize: 20,
        fontWeight: 'bold',
        lineHeight: 21,
        textAlign: 'center',
        marginBottom: 5,
        color: '#333'
    },
})

export default Header
