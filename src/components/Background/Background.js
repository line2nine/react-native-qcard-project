import React from 'react'
import {ImageBackground, StyleSheet, KeyboardAvoidingView, View, Image} from 'react-native'
import {theme} from '../../core/theme'

const Background = ({children}) => (
    <View
        style={styles.background}
    >
        <View style={styles.container}>
            {children}
        </View>
    </View>
)

const styles = StyleSheet.create({
    background: {
        flex: 1,
        width: '100%',
        backgroundColor: '#fff',
    },
    container: {
        flex: 1,
        padding: 30,
        width: '100%',
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
    ads: {
        // flex: 1,
        padding: 5,
        // width: 336,
        // height: 280,
        // alignSelf: 'center',
        alignItems: 'center',
        // justifyContent: 'center',
    }
})

export default Background
