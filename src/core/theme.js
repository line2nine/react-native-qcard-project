import {DefaultTheme} from 'react-native-paper'

export const theme = {
    ...DefaultTheme,
    colors: {
        ...DefaultTheme.colors,
        text: '#555',
        primary: '#555',
        secondary: '#eeeeee',
        error: '#f13a59',
    },
}
