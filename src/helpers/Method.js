import axios from 'axios';

import * as Setting from './Setting';

export async function handleAuth(endpointApi, data) {
    try {
        const config = {
            headers: {
                Accept: "application/json",
                "Content-type": "application/json",
            }
        };
        let endpoint = Setting.URL + endpointApi;
        return await axios.post(endpoint, data, config);
    } catch (error) {
        console.log(error);

    }
}

export async function handleGet(endpointApi, token) {
    try {
        const config = {
            headers: {
                Accept: "application/json",
                "Content-type": "application/json",
                Authorization: `Bearer ${token}`
            }
        };
        let endpoint = Setting.URL + endpointApi;
        return await axios.get(endpoint, config);
    } catch (error) {
        console.log(error);

    }

}

export async function handleSubmit(endpointApi, data, token) {
    try {
        const config = {
            headers: {Authorization: `Bearer ${token}`}
        };
        let endpoint = Setting.URL + endpointApi;
        return await axios.post(endpoint, data, config);
    } catch (error) {
        console.log(error);

    }
}

export async function handleUpdate(endpointApi, data, token) {
    try {
        const config = {
            headers: {Authorization: `Bearer ${token}`}
        };
        let endpoint = Setting.URL + endpointApi;
        return await axios.put(endpoint, data, config);
    } catch (error) {
        console.log(error);

    }
}

export async function handleDelete(endpointApi, token) {
    try {
        const config = {
            headers: {Authorization: `Bearer ${token}`}
        };
        let endpoint = Setting.URL + endpointApi;
        return await axios.delete(endpoint, config);
    } catch (error) {
        console.log(error);

    }
}